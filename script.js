$('#loading').show();
$('#main').hide();
$('.link_area_container').hide();

$('html, body').css({
  'overflow-y': 'hidden'
});

indicator = "　→";
replaceSpace = "　　"
$(function () {
  logoBlink();

  $("#default_tab").trigger("click");

  $('html, body').css({
    'overflow-y': 'initial'
  });
  $(".sidebar_item").each(function(i, elem) {
    if (i == 0) return;
    $(elem).text($(elem).text() + replaceSpace)
  })
});

document.fonts.onloadingdone = function (fontFaceSetEvent) {
  $('#loading').hide();
  $('#main').show();
  $('.link_area_container').show();
};

var text_in_span = 30;
var blink_keytime = 0.7;
var blink_count = 25;

function logoBlink() {
  $('.type_text').children().addBack().contents().each(function () {
    if (this.nodeType == 3) {
      $(this).replaceWith($(this).text().replace(/(\S)/g, '<span>$1</span>'));
    }
  });

  $('.type_text').css({
    'opacity': 1
  });

  var length = $('.type_text').children().length;
  for (var i = 0; i <= length; i++) {
    $('.type_text').children('span:eq(' + i + ')').delay(text_in_span * i).animate({
      'opacity': 1
    }, 0);
  }

  setTimeout(function () {
    for (var j = 0; j < blink_count; j++) {
      $('.type_text').delay(blink_keytime * 2 * j).animate({
        'opacity': 0
      }, 0);

      $('.type_text').delay((blink_keytime * 2 * j) + blink_keytime).animate({
        'opacity': 1
      }, 0);
    }
  }, 300);
}




function switchContent(evt, button, key) {
  var i, tabcontent, tablinks;

  $(".tab_content").each(function (i, elem) {
    $(elem).removeClass("show");
  })
  $(".tab_content" + "#" + key).addClass("show");

  tablinks = $(".tab_item");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  button.className += " active";



  if (key === "works") {
    $("#hypersignal").trigger("click");
  } else {}
}


var currentWorksTab = "hypersignal";

function switchWorksContent(event, button, key) {
  currentWorksTab = button.id;

  $('#works_content').children().each(function (i, elem) {
    $(elem).removeClass("show");
  });
  console.log($("#" + key));
  $("#" + key).addClass("show");


  $(".sidebar_item").each(function (i, elem) {
    $(elem).text($(elem).text().replace(indicator, replaceSpace));
    $(elem).removeClass("active");
  })
  if ($(button).text().match(new RegExp(replaceSpace)))
    $(button).text($(button).text().replace(replaceSpace, indicator));
  else
    $(button).text($(button).text() + indicator);
  $(button).addClass("active");

  
  $('.iframe').each(function(i, elem) {
    var src = elem.attr("src");
    elem.attr("src","");
    elem.attr("src",src);
  });
}